'use strict';

var app = {

    initialize: function () {
        document.addEventListener('deviceready', this.onDeviceReady.bind(this), false);
    },

    running: false,

    /**
     * Device ready event handler
     *
     * Bind any Cordova events here. Common events are: 'pause', 'resume', etc.
     */
    onDeviceReady: function () {
        document.addEventListener('pause', app.onPause, false);
        document.getElementById('startLocalization').onclick = app.start;
        document.getElementById('stopLocalization').onclick = app.stop;
    },

    onPause: function () {
        if (app.running) {
            app.stop();
        }
    },
    
    start: function () {
        console.log('Starting Localization');
        Navigation.startLocalization(
            'xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx', // <- PUT YOUR API KEY HERE
            123456789, // <- PUT YOUR BUILDING IDENTIFIER HERE
            app.indoorsListener,
            successCallback.bind(null, 'startLocalization'),
            errorCallback.bind(null, 'startLocalization')
        );
    },

    stop: function () {
        console.log('Stopping Localization');
        app.running = false;
        Navigation.stopLocalization(
            successCallback.bind(null, 'stopLocalization'),
            errorCallback.bind(null, 'stopLocalization')
        );
    },

    indoorsListener: {
        buildingLoaded: function(building) {
            app.running = true;
            addJSONLog("buildingLoaded", building);
            document.getElementById("buildingPanel").className = ""
            var buildingInfo = document.getElementById("buildingInfo");
            buildingInfo.innerHTML = JSON.stringify(building, null, 2);
            document.getElementById('loadingPanel').className = 'hidden';
            Navigation.getVersion(app.versionCallback, errorCallback.bind(null, 'getVersion'))
        },
        positionUpdated: function (position) {
            addJSONLog('positionUpdated', position);
            Navigation.toGeoLocation(
                position,
                addJSONLog.bind(null, 'toGeoLocation'),
                errorCallback.bind(null, 'toGeoLocation')
            );
        },
        enteredZones: function (result) {

        },
        changedFloor: addJSONLog.bind(null, 'changedFloor'),
        loadingBuilding: function (result) {
            document.getElementById('loadingPanel').className = '';
            document.getElementById('loading').innerHTML = result.progress
        },
        orientationUpdated: function (result) {
            document.getElementById('orientation').innerHTML = result.orientation
        }
    },

    versionCallback: function (result) {
        document.getElementById('version-sdk').innerHTML = result.version
    }
};

function successCallback(functionName, result) {
    var msg = 'Success';
    if (result) {
        msg += ': ' + JSON.stringify(result);
    }

    addJSONLog(functionName + '-success', msg);
}

function errorCallback(functionName, err) {
    if (err) {
        alert('Error: ' + JSON.stringify(err));
    }

    addJSONLog(functionName + '-error', err)
}

function addJSONLog(callbackName, result) {
    var message = callbackName;
    if (result) {
        message = message + ' - ' + JSON.stringify(result);
    }
    addLog(message);
}

function addLog(message) {
    var logElement = document.getElementById('log');
    var time = new Date().toLocaleTimeString('en-US', {
        hour12: false,
        hour: 'numeric',
        minute: 'numeric',
        second: 'numeric'
    });
    var timed_message = time + ': ' + message;
    console.log(timed_message);
    logElement.innerHTML = timed_message + '\n' + logElement.innerHTML;
}

app.initialize();
