Cordova Navigation Plugin Test Application
==========================================

## Set Environment variables

* ANDROID_HOME
* JAVA_HOME

## Clone

```
git clone https://bitbucket.org/indoors/cordova-navigation-test-app-public.git cordova-navigation-test-app
git clone https://bitbucket.org/indoors/cordova-navigation-plugin-public.git cordova-navigation-plugin
```

## Prepare/run project

```
cd cordova-navigation-test-app
cordova prepare
cordova run
```

## Update cordova plugin

```
cordova plugin rm cordova-plugin-indoors-navigation
cordova prepare
```